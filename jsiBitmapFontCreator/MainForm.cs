﻿/*
 * Created by SharpDevelop.
 * User: johan
 * Date: 2012-07-04
 * Time: 11:08
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using System.Windows.Forms;

namespace jsiBitmapFontCreator
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		private bool isInitializing = true;
        private string _fileNameBase;
        private string _saveFolder;
        private Color _shadowColor;

		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			
		}
		
		private void readSettings()
		{
			var fonts = new InstalledFontCollection();
			int foo = 0;
			foreach(var ff in fonts.Families)
			{
				if(ff.Name == Settings1.Default.FontName) foo = lstFonts.Items.Count;
				lstFonts.Items.Add(ff.Name);
			}
			lstFonts.SelectedIndex = foo;
			setForeColor(Settings1.Default.Forecolor);
			setBackColor(Settings1.Default.BackColor);
			setShadowColor(Settings1.Default.ShadowColor);
			setfontSize(Settings1.Default.Size);
			
			isInitializing = false;
		}

		private void writeSettings()
		{
			
		}
		
		private void setfontSize(int size)
		{
			sizeUpDown.Value = size;
			
		}
		private void setForeColor(Color color)
		{
			foreColorLabel.BackColor = color;
			picture32.ForeColor = color;
			picture128.ForeColor = color;
		}
		private void setBackColor(Color color)
		{
			backColorLabel.BackColor = color;
			picture32.BackColor = color;
			picture128.BackColor = color;
		}

		
		private void setShadowColor(Color color)
		{
			shadowColorLabel.BackColor = color;
		}
		
		private void drawFont32()
		{	
			string sub = "";
			var b = createBitmap(32, 128, true, out sub);
			picture32.BackColor = backColorLabel.BackColor;
			picture32.Image = b;
			Debug.WriteLine(sub);

		}
		
		private void drawFont128()
		{
			string sub = "";
			var b = createBitmap(128, 256, true, out sub);
			picture128.BackColor = backColorLabel.BackColor;
			picture128.Image = b;
			Debug.WriteLine(sub);
		}

		private Bitmap createBitmap(int startChar, int endChar, bool preview, out string subimage)
		{
			int x = 0;
			int maxX = 0;
			int y = 0;
			int count = 0;
			subimage = "";
			var bitmap = new Bitmap(1,1 );
			var g = Graphics.FromImage(bitmap);

			var style = new FontStyle();
			style = FontStyle.Regular;


            var f = new Font(new FontFamily(lstFonts.SelectedItem.ToString()), (float)sizeUpDown.Value, style);
            var sbrush = new SolidBrush(shadowColorLabel.BackColor);
            var brush = new SolidBrush(foreColorLabel.BackColor);

			var fs = g.MeasureString("M", f);
			bitmap = new Bitmap(bitmap,(int)fs.Width*16 , (int)fs.Height* ((endChar - startChar)/16) + 3);
			g = Graphics.FromImage(bitmap);
			
			if(preview)
			{
				g.Clear(backColorLabel.BackColor);
			}
			else
			{
				g.Clear(Color.Transparent);
			}
			
			g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
    		g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

			for(int i = startChar; i < endChar; i++)
			{
				byte b = (byte)i;
				//b = 65;
				string s = System.Text.Encoding.GetEncoding("Windows-1252").GetString(new byte[]{b}, 0, 1);
				fs = g.MeasureString(s, f);
                g.DrawString(s, f, sbrush, x + (int)xOffsetUpDown1.Value, y + (int)yOffsetUpDown2.Value);
                g.DrawString(s, f, brush, x, y);
				int w = (int)Math.Ceiling(fs.Width);
				int h = (int)Math.Ceiling(fs.Height);
				subimage += string.Format("{0}:{1}:{2}:{3}:{4}", i.ToString(), x, y, w, h) + Environment.NewLine;


				x += w;
				count++;
				
				if(count == 16)
				{
					if(x > maxX) maxX = x;
					
					count=0;
					x = 0;
					fs = g.MeasureString("T", f);
					y += h;
				}
			}
			g.Flush();
			f.Dispose();
			g.Dispose();
			
			var rect = new Rectangle(0, 0, (int)maxX > bitmap.Width ? bitmap.Width : (int)maxX, (int)y > bitmap.Height ? bitmap.Height : (int)y);
		   Bitmap bmpCrop = bitmap.Clone(rect,   bitmap.PixelFormat);

			return bmpCrop;
		}

		private void drawFont()
		{
			drawFont32();
			drawFont128();

            _fileNameBase = lstFonts.SelectedItem.ToString() + "_" + sizeUpDown.Value.ToString();
		}
		
		void MainFormLoad(object sender, EventArgs e)
		{
			readSettings();
			drawFont();
		}
		
		void Picture32Paint(object sender, PaintEventArgs e)
		{
			//drawFont32();
		}
		
		void Picture128Paint(object sender, PaintEventArgs e)
		{
			//drawFont128();
		}
		
		void LstFontsSelectedIndexChanged(object sender, EventArgs e)
		{
			if(isInitializing) return;
			
			drawFont();
		}
		

		
		
		

        private void fontColorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                setForeColor(colorDialog1.Color);
                drawFont();
            }

        }

        private void shadowColorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                setShadowColor(colorDialog1.Color);
                drawFont();
            }

        }

        private void sizeUpDown_ValueChanged(object sender, EventArgs e)
        {
            drawFont();
        }

        private void previewColorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                setBackColor(colorDialog1.Color);
                drawFont();
            }

        }

        private void xOffsetUpDown1_ValueChanged(object sender, EventArgs e)
        {
            drawFont();
        }

        private void yOffsetUpDown2_ValueChanged(object sender, EventArgs e)
        {
            drawFont();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {

            _saveFolder = folderBrowserDialog1.SelectedPath;

            string s1, s2;
            var b1 = createBitmap(32, 128, false, out s1);
            var b2 = createBitmap(128, 256, false, out s2);

            File.WriteAllText(Path.Combine(_saveFolder, _fileNameBase + "_low subimages.txt"), s1);
            b1.Save(Path.Combine(_saveFolder, _fileNameBase + "_low.png"), ImageFormat.Png);
            File.WriteAllText(Path.Combine(_saveFolder, _fileNameBase + "_high subimages.txt"), s2);
            b2.Save(Path.Combine(_saveFolder, _fileNameBase + "_high.png"), ImageFormat.Png);
            Settings1.Default.SaveFolder = _saveFolder;

            MessageBox.Show("Done.", "Save");

        }
    }
}
