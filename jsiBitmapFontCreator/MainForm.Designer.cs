﻿/*
 * Created by SharpDevelop.
 * User: johan
 * Date: 2012-07-04
 * Time: 11:08
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace jsiBitmapFontCreator
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lstFonts = new System.Windows.Forms.ListBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.picture32 = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.picture128 = new System.Windows.Forms.PictureBox();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.SaveButton = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.previewColorButton = new System.Windows.Forms.Button();
            this.shadowColorButton = new System.Windows.Forms.Button();
            this.fontColorButton = new System.Windows.Forms.Button();
            this.backColorLabel = new System.Windows.Forms.Label();
            this.shadowColorLabel = new System.Windows.Forms.Label();
            this.foreColorLabel = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.fontUnderlined = new System.Windows.Forms.CheckBox();
            this.fontItalic = new System.Windows.Forms.CheckBox();
            this.fontBold = new System.Windows.Forms.CheckBox();
            this.sizeUpDown = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.xOffsetUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.yOffsetUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picture32)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picture128)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sizeUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xOffsetUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yOffsetUpDown2)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.lstFonts);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(209, 472);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Your Fonts";
            // 
            // lstFonts
            // 
            this.lstFonts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstFonts.FormattingEnabled = true;
            this.lstFonts.IntegralHeight = false;
            this.lstFonts.Location = new System.Drawing.Point(3, 16);
            this.lstFonts.Name = "lstFonts";
            this.lstFonts.Size = new System.Drawing.Size(203, 453);
            this.lstFonts.TabIndex = 0;
            this.lstFonts.SelectedIndexChanged += new System.EventHandler(this.LstFontsSelectedIndexChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(218, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(668, 260);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.picture32);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(660, 234);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "32 - 127";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // picture32
            // 
            this.picture32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picture32.Location = new System.Drawing.Point(3, 3);
            this.picture32.Name = "picture32";
            this.picture32.Size = new System.Drawing.Size(654, 228);
            this.picture32.TabIndex = 0;
            this.picture32.TabStop = false;
            this.picture32.Paint += new System.Windows.Forms.PaintEventHandler(this.Picture32Paint);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.picture128);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(660, 245);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "128 - 256";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // picture128
            // 
            this.picture128.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picture128.Location = new System.Drawing.Point(3, 3);
            this.picture128.Name = "picture128";
            this.picture128.Size = new System.Drawing.Size(654, 239);
            this.picture128.TabIndex = 0;
            this.picture128.TabStop = false;
            this.picture128.Paint += new System.Windows.Forms.PaintEventHandler(this.Picture128Paint);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.groupBox3);
            this.groupBox4.Controls.Add(this.groupBox2);
            this.groupBox4.Location = new System.Drawing.Point(218, 267);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(668, 203);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            // 
            // SaveButton
            // 
            this.SaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SaveButton.Location = new System.Drawing.Point(138, 131);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(50, 23);
            this.SaveButton.TabIndex = 11;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.yOffsetUpDown2);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.xOffsetUpDown1);
            this.groupBox3.Controls.Add(this.shadowColorButton);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.shadowColorLabel);
            this.groupBox3.Location = new System.Drawing.Point(212, 11);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 164);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Shadow";
            // 
            // previewColorButton
            // 
            this.previewColorButton.Location = new System.Drawing.Point(6, 20);
            this.previewColorButton.Name = "previewColorButton";
            this.previewColorButton.Size = new System.Drawing.Size(127, 23);
            this.previewColorButton.TabIndex = 4;
            this.previewColorButton.Text = "Preview Background";
            this.previewColorButton.UseVisualStyleBackColor = true;
            this.previewColorButton.Click += new System.EventHandler(this.previewColorButton_Click);
            // 
            // shadowColorButton
            // 
            this.shadowColorButton.Location = new System.Drawing.Point(6, 131);
            this.shadowColorButton.Name = "shadowColorButton";
            this.shadowColorButton.Size = new System.Drawing.Size(127, 23);
            this.shadowColorButton.TabIndex = 3;
            this.shadowColorButton.Text = "Color";
            this.shadowColorButton.UseVisualStyleBackColor = true;
            this.shadowColorButton.Click += new System.EventHandler(this.shadowColorButton_Click);
            // 
            // fontColorButton
            // 
            this.fontColorButton.Location = new System.Drawing.Point(10, 131);
            this.fontColorButton.Name = "fontColorButton";
            this.fontColorButton.Size = new System.Drawing.Size(127, 23);
            this.fontColorButton.TabIndex = 0;
            this.fontColorButton.Text = "Color";
            this.fontColorButton.UseVisualStyleBackColor = true;
            this.fontColorButton.Click += new System.EventHandler(this.fontColorButton_Click);
            // 
            // backColorLabel
            // 
            this.backColorLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.backColorLabel.Location = new System.Drawing.Point(143, 20);
            this.backColorLabel.Name = "backColorLabel";
            this.backColorLabel.Size = new System.Drawing.Size(45, 23);
            this.backColorLabel.TabIndex = 2;
            // 
            // shadowColorLabel
            // 
            this.shadowColorLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.shadowColorLabel.Location = new System.Drawing.Point(149, 131);
            this.shadowColorLabel.Name = "shadowColorLabel";
            this.shadowColorLabel.Size = new System.Drawing.Size(45, 23);
            this.shadowColorLabel.TabIndex = 1;
            // 
            // foreColorLabel
            // 
            this.foreColorLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.foreColorLabel.Location = new System.Drawing.Point(143, 131);
            this.foreColorLabel.Name = "foreColorLabel";
            this.foreColorLabel.Size = new System.Drawing.Size(45, 23);
            this.foreColorLabel.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.fontUnderlined);
            this.groupBox2.Controls.Add(this.fontItalic);
            this.groupBox2.Controls.Add(this.fontColorButton);
            this.groupBox2.Controls.Add(this.fontBold);
            this.groupBox2.Controls.Add(this.sizeUpDown);
            this.groupBox2.Controls.Add(this.foreColorLabel);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(6, 11);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 164);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Font";
            // 
            // fontUnderlined
            // 
            this.fontUnderlined.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.fontUnderlined.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fontUnderlined.Location = new System.Drawing.Point(7, 101);
            this.fontUnderlined.Name = "fontUnderlined";
            this.fontUnderlined.Size = new System.Drawing.Size(100, 24);
            this.fontUnderlined.TabIndex = 4;
            this.fontUnderlined.Text = "Underline";
            this.fontUnderlined.UseVisualStyleBackColor = true;
            // 
            // fontItalic
            // 
            this.fontItalic.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.fontItalic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fontItalic.Location = new System.Drawing.Point(7, 71);
            this.fontItalic.Name = "fontItalic";
            this.fontItalic.Size = new System.Drawing.Size(100, 24);
            this.fontItalic.TabIndex = 3;
            this.fontItalic.Text = "Italic";
            this.fontItalic.UseVisualStyleBackColor = true;
            // 
            // fontBold
            // 
            this.fontBold.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.fontBold.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fontBold.Location = new System.Drawing.Point(7, 45);
            this.fontBold.Name = "fontBold";
            this.fontBold.Size = new System.Drawing.Size(100, 24);
            this.fontBold.TabIndex = 2;
            this.fontBold.Text = "Bold";
            this.fontBold.UseVisualStyleBackColor = true;
            // 
            // sizeUpDown
            // 
            this.sizeUpDown.Location = new System.Drawing.Point(60, 18);
            this.sizeUpDown.Name = "sizeUpDown";
            this.sizeUpDown.Size = new System.Drawing.Size(47, 20);
            this.sizeUpDown.TabIndex = 1;
            this.sizeUpDown.ValueChanged += new System.EventHandler(this.sizeUpDown_ValueChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Size";
            // 
            // xOffsetUpDown1
            // 
            this.xOffsetUpDown1.Location = new System.Drawing.Point(59, 18);
            this.xOffsetUpDown1.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.xOffsetUpDown1.Name = "xOffsetUpDown1";
            this.xOffsetUpDown1.Size = new System.Drawing.Size(47, 20);
            this.xOffsetUpDown1.TabIndex = 6;
            this.xOffsetUpDown1.ValueChanged += new System.EventHandler(this.xOffsetUpDown1_ValueChanged);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(6, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 5;
            this.label2.Text = "X offset";
            // 
            // yOffsetUpDown2
            // 
            this.yOffsetUpDown2.Location = new System.Drawing.Point(59, 48);
            this.yOffsetUpDown2.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.yOffsetUpDown2.Name = "yOffsetUpDown2";
            this.yOffsetUpDown2.Size = new System.Drawing.Size(47, 20);
            this.yOffsetUpDown2.TabIndex = 8;
            this.yOffsetUpDown2.ValueChanged += new System.EventHandler(this.yOffsetUpDown2_ValueChanged);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(6, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 23);
            this.label3.TabIndex = 7;
            this.label3.Text = "Y offset";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.previewColorButton);
            this.groupBox5.Controls.Add(this.SaveButton);
            this.groupBox5.Controls.Add(this.backColorLabel);
            this.groupBox5.Location = new System.Drawing.Point(418, 11);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(200, 164);
            this.groupBox5.TabIndex = 9;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Other";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(893, 482);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox1);
            this.Name = "MainForm";
            this.Text = "jsiBitmapFontCreator";
            this.Load += new System.EventHandler(this.MainFormLoad);
            this.groupBox1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picture32)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picture128)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sizeUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xOffsetUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yOffsetUpDown2)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
		private System.Windows.Forms.PictureBox picture128;
		private System.Windows.Forms.PictureBox picture32;
		private System.Windows.Forms.ColorDialog colorDialog1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.ListBox lstFonts;
		private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button previewColorButton;
        private System.Windows.Forms.Button shadowColorButton;
        private System.Windows.Forms.Button fontColorButton;
        private System.Windows.Forms.Label backColorLabel;
        private System.Windows.Forms.Label shadowColorLabel;
        private System.Windows.Forms.Label foreColorLabel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox fontUnderlined;
        private System.Windows.Forms.CheckBox fontItalic;
        private System.Windows.Forms.CheckBox fontBold;
        private System.Windows.Forms.NumericUpDown sizeUpDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.NumericUpDown yOffsetUpDown2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown xOffsetUpDown1;
        private System.Windows.Forms.Label label2;
    }
}
